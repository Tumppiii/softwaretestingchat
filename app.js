/**
 * Represents a server.
 * Necessary for tests to work.
 *
 * @constructor
 */
function App() {};

// Testing variables
App.usercount = 0;
App.username = 'Anonymous';
App.isTyping = false;

// Modules
const express = require('express');
const fs = require('fs');
const readline = require('readline');

// Variables
var app = express();
var port = 4343;
var file = 'data.txt';
var read;

// Set the template engine ejs
app.set('view engine', 'ejs');

// Middlewares
app.use(express.static('public'));

// Ruote
app.get('/', function(req, res) {
  res.render('index');
});

// Listen on port
server = app.listen(port);
console.log('Server listening on localhost:'+port);

// socket.io instantiation
io = require("socket.io")(server);

/**
 * Start listening new socket connections.
 * This stays active as long as the program runs.
 *
 * @param {socket} socket - Client socket variable.
 */
io.on('connection', function(socket) {
  // Default socket username
  socket.username = App.username;

  /**
   * Triggers when new socket is connected.
   *
   * Adds one user to usercount.
   * Displays data on console.
   *
   * Reads every message from data.txt file and
   * sends them to socket one by one.
   */
  socket.on('connected', function() {
    App.usercount++;
    console.log('New user connected | User count: '+App.usercount);

    // Define filestream
    read = readline.createInterface({input: fs.createReadStream(file)});
    /**
     * Read every line from data.txt and
     * send them to socket one by one.
     *
     * @param {string} line - One line from data.txt
     */
    read.on('line', function (line) {
      socket.emit('memory', {message : line});
    });
  });

  /**
   * Triggers when socket is disconnected.
   *
   * Removes one user from usercount.
   * Displays data on console.
   *
   * @param {socket} socket - Client socket variable
   */
  socket.on('disconnect', function(socket) {
    App.usercount--;
    console.log('User disconnected | User count: '+App.usercount);
  });

  /**
   * Triggers when socket calls change_username.
   *
   * Changes old username to new username.
   * Displays data on console.
   *
   * @param {data} data - Contains old and new username.
   */
  socket.on('change_username', function(data) {
    socket.username = data.newname;
    App.username = data.newname;
    console.log(data.oldname+' changed name to '+data.newname);
  });

  /**
   * Triggers when socket calls new_message.
   *
   * Changes isTyping variable to false.
   * Saves message into App.message for testing.
   * Broadcasts received message and sender to all sockets.
   * Saves received message and sender in data.txt file.
   *
   * @param {data} data - Contains send message and username.
   */
  socket.on('new_message', function(data) {
    App.isTyping = false;
    App.message = data.message;

    // Broadcast the new message
    io.sockets.emit('new_message', {message : data.message, username : socket.username});

    // Save new message to file
    fs.appendFile(file, socket.username+": "+data.message+"\n", function (err) {
      if (err) throw err;
    });
  });

  /**
   * Triggers when one socket emits typing.
   *
   * Changes isTyping variable to true.
   * Emits typing to all other sockets.
   *
   * @param {data} data - Contains socket username.
   */
  socket.on('typing', function(data) {
    App.isTyping = true;
    socket.broadcast.emit('typing', {username : socket.username});
  });
});

// Module export for jasmine testing
module.exports = App;
