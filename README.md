# Chatter

Chatter is a web application that provides a possibility to chat with other users on a website without any unnecessary hassle.

## Getting started

In order to get started with it you will need the following

* NodeJS: [Download](https://nodejs.org/en/)

* Git: [Download](https://git-scm.com/)

*The examples given in this readme also use the npm package manager.*

*Installing NodeJS automatically installs the npm package manager to your system.*

*In order to run NodeJS and Git, the system needs to be restarted.*

You will also need the following Git repository:

`git clone https://bitbucket.org/Tumppiii/softwaretestingchat.git`

Or alternatively download the repository as a zip file

`https://bitbucket.org/Tumppiii/softwaretestingchat/get/07fe60a49114.zip`

## Installing

Once you have the items mentioned above you can start using the application.
Simply open up your preferred command line interface and navigate into the project folder.

Using cmd it should look something similiar to this:

`C:\Users\User>cd Drive\Users\User\softwaretestingchat`

*To be sure that you made it into the right folder the starting line should now end with 'softwaretestingchat'. Here is an example of it:
`C:\Users\Example\softwaretestingchat>`*

To test that the app starts running correctly you can enter the "npm start" in the command line.

`C:\Users\Example\softwaretestingchat>npm start`

To make sure the application runs correctly, the command line should tell the following.

`Server listening on localhost:4343`

*If you encounter any problems during this part, make sure that the path given in the command line is the same as the path to the 'softwaretestingchat' folder*

If the command line shows the line mentioned above, you should be able to go to the web address on any browser and start using the application.

In order to close the application, select command line interface and double press **ctrl+c**.

## Running tests

The software uses Jasmine framework to test the code and to produce a test report in HTML form automatically. The test report HTML file (named *report*) can be found in the 'testreport' folder inside 'softwaretestingchat'.

In order to run the tests on your system you can simply use the command "npm test" in the command line interface.

`C:\Users\Example\softwaretestingchat>npm test`

*During testing you should also see some information of the tests running in the command line with the conclusion in the end.*

As mentioned above, the tests use Jasmine framework. All of the tests are written in a *appSpec.js* file located in 'spec' folder. The *bitbucket-pipelines.yml* file has also the command 'npm test' to run the tests automatically on each push.

Here's an example of a test testing the applications chat history feature to make sure that the text sent by an user is saved correctly to the txt file.
```
it('server should send and save message', function(done) {
      socket.emit('new_message', {message : 'message'})
      setTimeout(function() {
        expect(App.message).toBe('message');
        readlast.read('data.txt', 1)
        .then((lines) => expect(lines).toBe('Anonymous: message\n'));
        done();
      }, 100);
  });
```
The timeouts are used in order to make sure that the server has enough time to register the actions correctly. This allows for more consistant test results and **should be used** when writing tests that uses the server.

## Deployment

In order to run this web app on another web address, you can simply change this address in the code in *chat.js* file located in 'public' folder.
```
line 7   var socket = io('http://localhost:4343');
```
During the writing of this the application is also running in the following address:

[saitti.ddns.net:4343](saitti.ddns.net:4343)

*this address may be blocked by some networks*

## Built With

* [NodeJS](https://nodejs.org/en/)
* [Express](https://expressjs.com/)
* [Socket.IO](https://socket.io/)

## Authors

- **Toni Anttila**
- **Tuomas Hirvonen**

## License

This project is licensed under the ISC License - see [ISC License](https://choosealicense.com/licenses/isc/) for details.
