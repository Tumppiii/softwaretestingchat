var Jasmine = require('jasmine');
var HtmlReporter = require('jasmine-pretty-html-reporter').Reporter;
var jasmine = new Jasmine();
var path = require('path');

jasmine.loadConfigFile('./spec/support/jasmine.json');

// Options object
jasmine.addReporter(new HtmlReporter({
  path: path.join(__dirname, 'testreport')
}));

jasmine.execute();
