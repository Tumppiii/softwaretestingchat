// Modules
const App = require('../app.js');
const client = require('socket.io-client');
const readlast = require('read-last-lines');

// Variables
var app;

// -----------------------------------------------------------------------------

describe('App', function() {
  /**
   * Executes only before all tests.
   *
   * Creates a new server.
   */
  beforeAll(function() {
    app = new App();
  });

  /**
   * Executes before each test.
   *
   * Makes a new socket.
   * Connects new socket to server.
   * Emits connected status to server.
   *
   * @param {callback} done - When done() is called, function ends.
   */
  beforeEach(function(done) {
    socket = client.connect('http://localhost:4343');
    socket.on('connect', function() {
      socket.emit('connected', function() {});
      done();
    });
  });

  /**
   * Executes after every test.
   *
   * If socket is connected(it should be)
   * Disconnects socket from server.
   *
   * @param {callback} done - When done() is called, function ends.
   */
  afterEach(function(done) {
    if(socket.connected) {
      socket.disconnect();
    };
    done();
  });

  /**
   * TEST 1. Client should connet to server.
   *
   * Usercount should be 1 when only one client is connected.
   *
   * Without setTimeout() server doesn't have enough time
   * to reqister new socket.
   *
   * Test fails if any sockets are open outside test.
   *
   * @param {callback} done - When done() is called, function ends.
   */
  it('client should connect to server', function(done) {
    console.log('STARTING: client should connect to server');
    setTimeout(function() {
      expect(App.usercount).toBe(1);
      console.log('ENDING: client should connect to server');
      done();
    }, 100);
  });

  /**
   * TEST 2. Server should change username.
   *
   * Username should be "Anonymous" before changing it.
   * Socket emits new username to server.
   * After emitting new username should be "newuser".
   *
   * Without setTimeout() server doesn't have enough time
   * to register new username.
   *
   * @param {callback} done - When done() is called, function ends.
   */
  it('server should change username', function(done) {
      console.log('STARTING: server should change username');
      expect(App.username).toBe('Anonymous');
      socket.emit('change_username', {newname : 'newuser'});
      setTimeout(function() {
        expect(App.username).toBe('newuser');
        App.username = 'Anonymous';
        console.log('ENDING: server should change username');
        done();
      }, 100);
  });

  /**
   * TEST 3. Server should send and save message.
   *
   * Socket emits new message to server.
   * Server reads the message and it should be 'message'.
   * Server reads last line from data.txt and
   * checks if it is the same as sended message.
   *
   * Without setTimeout() server doesn't have enough time
   * to register and read new message.
   *
   * @param {callback} done - When done() is called, function ends.
   */
  it('server should send and save message', function(done) {
      console.log('STARTING: server should send and save a message');
      socket.emit('new_message', {message : 'message'})
      setTimeout(function() {
        expect(App.message).toBe('message');
        readlast.read('data.txt', 1)
        .then((lines) => expect(lines).toBe('Anonymous: message\n'));
        console.log('ENDING: server should send and save a message');
        done();
      }, 100);
  });

  /**
   * TEST 4. Server should emit typing.
   *
   * Before socket emits typing, isTyping variable should be false.
   * Socket emits typing to server.
   * After emiting typing, isTyping variable should be true.
   *
   * Without setTimeout() server doesn't have enough time
   * to register typing.
   *
   * @param {callback} done - When done() is called, function ends.
   */
  it('server should emit typing', function(done) {
      console.log('STARTING: server should emit typing');
      expect(App.isTyping).toBe(false);
      socket.emit('typing');
      setTimeout(function() {
        expect(App.isTyping).toBe(true);
        console.log('ENDING: server should emit typing');
        done();
      }, 100);
  });
});
