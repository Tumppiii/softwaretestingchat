/**
 * jquery function.
 * Contains all client functionality.
 */
$(function() {
  // Define url for socket
  var socket = io('http://localhost:4343'); // http://saitti.ddns.net:4343

  // Buttons and inputs
  var message = $("#message");
  var username = $("#username");
  var current_username = $("#current_username");
  var send_message = $("#send_message");
  var send_username = $("#send_username");
  var chatroom = $("#chatroom");
  var feedback = $("#feedback");

  // Some elements
  var textinput = document.getElementById("message");
  var usernameinput = document.getElementById("username");
  var textbox = document.getElementById("chatroom");

  /**
   * Triggers when new socket is connected.
   *
   * Emits connected status to server.
   * Changes default username value to "Anonymous".   *
   */
  socket.on('connect', function() {
    socket.emit('connected', function() {});
    current_username.value = "Anonymous";
  });

  /**
   * Triggers when server calls memory.
   *
   * Is called as many times as there is messages in memory.
   * Adds messages into chatroom.
   * Scrolls chatroom to bottom.
   *
   * @param {data} data - contains one message from memory.
   */
  socket.on("memory", function(data) {
    chatroom.append("<p class='message'>"+data.message+"</p>");
    textbox.scrollTop = textbox.scrollHeight;
  });

  /**
   * Triggers when send_message button is clicked.
   *
   * Emits message to server.
   * Clears new message input.
   */
  send_message.click(function() {
    socket.emit('new_message', {message : message.val()});
    document.getElementById('message').value = '';
  });

  /**
   * Calls send_message when cursor is in new_message box
   * and enter-key is pressed.
   *
   * @param {event} event - Event variable.
   */
  textinput.addEventListener("keyup", function(event) {
    if (event.keyCode == 13) {
      event.preventDefault();
      document.getElementById("send_message").click();
    };
  });

  /**
   * Triggers when send_username button is clicked.
   *
   * Emits old and new username to server.
   * Makes new username sockets username.
   * Displays new username.
   */
  send_username.click(function() {
    socket.emit('change_username', {oldname : current_username.value, newname : username.val()});
    current_username.value = username.val();
    document.getElementById('current_username').innerHTML = username.val();
  });

  /**
   * Calls send_username when cursor is in send_username box
   * and enter-key is pressed.
   *
   * @param {event} event - Event variable.
   */
  usernameinput.addEventListener("keyup", function(event) {
    if (event.keyCode == 13) {
      event.preventDefault();
      document.getElementById("send_username").click();
    };
  });

  /**
   * Triggers when server calls new_message.
   *
   * Clears is typing.. from top.
   * (Clears it even if someone else is still typing.. Needs improvement.)
   * Adds new message into chatroom.
   * Scrolls chatroom to bottom.
   *
   * @param {data} data - Contains message and username.
   */
  socket.on("new_message", function(data) {
    feedback.html('');
    chatroom.append("<p class='message'>"+data.username+": "+data.message+"</p>");
    textbox.scrollTop = textbox.scrollHeight;
  });

  /**
   * Triggers when client is typing.
   *
   * Emits typing to server.
   */
  message.bind("keypress", function() {
    socket.emit('typing');
  });

  /**
   * Triggers when server sends typing.
   *
   * Displays typers username.
   *
   * @param {data} data - Contains username.
   */
  socket.on('typing', function(data) {
    feedback.html("<p><i>" + data.username + " is typing a message..." + "</i></p>");
  });
});
